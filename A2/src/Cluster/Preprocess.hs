{-# LANGUAGE OverloadedStrings, RankNTypes #-}

module Cluster.Preprocess
( Pokemon (..)
, Boolean (..)
, Numeric (..)
, clean
, toList
, open
, store
, decode
, flattenAll
, flatten
, aFewFeatures
, aFewFeaturesButId
, allButPokemonId
, allFeatures
) where

import qualified Data.ByteString.Char8 as BChar
import Data.Maybe
import qualified Text.Read as Read
import qualified Data.List as List
import Control.Exception (IOException)
import qualified Control.Exception as Exception
import System.IO
import Control.Applicative
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as ByteString
import Data.Csv
  ( DefaultOrdered(headerOrder)
  , FromField(parseField)
  , FromNamedRecord(parseNamedRecord)
  , Header
  , ToField(toField)
  , ToNamedRecord(toNamedRecord)
  , (.:)
  , (.=)
  )
import qualified Data.Csv as Cassava
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import qualified Data.Foldable as Foldable

class Unwrap c where
  unwrap :: c -> Maybe Double

data Boolean = Boolean { getBScale :: Maybe Double } deriving (Show, Read, Eq)

instance Unwrap Boolean where
  unwrap = getBScale

instance FromField Boolean where
  parseField "true"  = pure . Boolean $ Just 1.0
  parseField "false" = pure . Boolean $ Just 2.0
  parseField _       = pure (Boolean Nothing)

data Numeric = Numeric { getNScale :: Maybe Double } deriving (Show, Read, Eq)

instance Unwrap Numeric where
  unwrap = getNScale

instance FromField Numeric where
  parseField str = (pure . Numeric) res
    where res = Read.readMaybe (BChar.unpack str) :: Maybe Double

data Pokemon = Pokemon
  { pokemon_id        :: !Numeric
  , latitude          :: !Numeric
  , longitude         :: !Numeric
  , hour              :: !Numeric
  , terrain           :: !Numeric
  , near_water        :: !Boolean
  , temperature       :: !Numeric
  , population        :: !Numeric
  , urban             :: !Boolean
  , suburban          :: !Boolean
  , midurban          :: !Boolean
  , rural             :: !Boolean
  , gym_distance      :: !Numeric
  , pokestop_distance :: !Numeric
  } deriving (Show, Read)

instance FromNamedRecord Pokemon where
  parseNamedRecord r = Pokemon
    <$> r .: "pokemonId"
    <*> r .: "latitude"
    <*> r .: "longitude"
    <*> r .: "appearedHour"
    <*> r .: "terrainType"
    <*> r .: "closeToWater"
    <*> r .: "temperature"
    <*> r .: "population_density"
    <*> r .: "urban"
    <*> r .: "suburban"
    <*> r .: "midurban"
    <*> r .: "rural"
    <*> r .: "gymDistanceKm"
    <*> r .: "pokestopDistanceKm"

store
 :: a
 -> a
store = id

clean
  :: [Pokemon]
  -> [Pokemon]
clean = List.filter (isJust . (flatten allFeatures))

aFewFeatures
  :: [Pokemon -> Maybe Double]
aFewFeatures =
  [ unwrap . pokemon_id
  , unwrap . gym_distance
  , unwrap . population
  , unwrap . temperature
  , unwrap . hour
  , unwrap . terrain ]

aFewFeaturesButId
  :: [Pokemon -> Maybe Double]
aFewFeaturesButId =
  [ unwrap . gym_distance
  , unwrap . population
  , unwrap . temperature
  , unwrap . hour
  , unwrap . terrain ]

allFeatures
  :: [Pokemon -> Maybe Double]
allFeatures =
  [ unwrap . pokemon_id
  , unwrap . latitude
  , unwrap . longitude
  , unwrap . hour
  , unwrap . terrain
  , unwrap . near_water
  , unwrap . temperature
  , unwrap . population
  , unwrap . urban
  , unwrap . suburban
  , unwrap . midurban
  , unwrap . rural
  , unwrap . gym_distance
  , unwrap . pokestop_distance ]

allButPokemonId
  :: [Pokemon -> Maybe Double]
allButPokemonId =
  [ unwrap . latitude
  , unwrap . longitude
  , unwrap . hour
  , unwrap . terrain
  , unwrap . near_water
  , unwrap . temperature
  , unwrap . population
  , unwrap . urban
  , unwrap . suburban
  , unwrap . midurban
  , unwrap . rural
  , unwrap . gym_distance
  , unwrap . pokestop_distance ]

open
  :: FilePath
  -> IO (Either String (Vector Pokemon))
open filepath =
  catchShowIO (ByteString.readFile filepath)
    >>= return . either Left decode

catchShowIO
  :: IO a
  -> IO (Either String a)
catchShowIO action =
  fmap Right action `Exception.catch` handle
  where handle
          :: IOException
          -> IO (Either String a)
        handle = return . Left . show

toList
  :: Vector Pokemon
  -> [Pokemon]
toList = Vector.toList

flattenAll
  :: [Pokemon -> Maybe Double]
  -> [Pokemon]
  -> [Maybe [Double]]
flattenAll fs pokemon = lift pokemon
  where lift = fmap (flatten fs)

flatten
  :: [Pokemon -> Maybe Double]
  -> Pokemon
  -> Maybe [Double]
flatten fs pokemon = sequenceA res
  where res = getZipList $ ZipList fs <*> ZipList (repeat pokemon)

decode
  :: ByteString
  -> Either String (Vector Pokemon)
decode =
  fmap snd . Cassava.decodeByName
