module Cluster.Algorithms
( hbu
, pam
, kmeans
, classify
) where

import Cluster.Utils
import Data.Random (MonadRandom)
import Data.List
  ( (\\)
  , sort
  , delete
  , nub
  , transpose
  , elemIndices
  )

classify
  :: (Ord a, Floating a)
  => [[a]]
  -> [a]
  -> [a]
classify (m:ms) example = nearest example m ms

hbu
  :: (Ord a, Floating a)
  => [[a]]
  -> [( [a], ([a], [a]) )]
hbu = hbu' []

hbu'
  :: (Ord a, Floating a)
  => [( [a], ([a], [a]) )]
  -> [[a]]
  -> [( [a], ([a], [a]) )]
hbu' splits [] = tail splits
hbu' splits centers = hbu' splits' centers'
  where location = closest_pair centers
        to       = merge location
        centers' = (to:centers) \\ [fst location, snd location]
        splits'  = (to, location):splits

pam
  :: (MonadRandom m, Ord a, Floating a)
  => [[a]]
  -> a
  -> Int
  -> m [[a]]
pam datapoints moe medoids = compute <$> sample medoids datapoints
  where compute = pam' datapoints moe medoids

pam'
  :: (Ord a, Floating a)
  => [[a]]
  -> a
  -> Int
  -> [[a]]
  -> [[a]]
pam' points moe medoid centers
  | converged = centers
  | otherwise = pam' points moe medoid centers'
    where converged     = has_converged moe (sort centers') (sort centers)
          medoids       = map (\p -> nearest p (head centers) (tail centers)) points
          (_, centers') = foldl(\(c, cs) (c', cs') -> min' c c' cs cs') (initial_cost, centers) products
          products      = [ co cand c | c <- centers, cand <- candidates ]
          initial_cost  = total_cost points centers
          candidates    = points \\ centers
          co cand c     = (total_cost points cs, cs)
            where cs = cand:(delete c centers)
          min' c c' cs cs'
            | c' < c    = (c', cs')
            | otherwise = (c, cs)

kmeans
  :: (MonadRandom m, Ord a, Floating a)
  => [[a]]
  -> a
  -> Int
  -> m [[a]]
kmeans datapoints moe clusters = compute <$> sample clusters datapoints
  where compute = kmeans' datapoints moe clusters

kmeans'
  :: (Ord a, Floating a)
  => [[a]]
  -> a
  -> Int
  -> [[a]]
  -> [[a]]
kmeans' points moe cluster (mean:means)
  | converged = means'
  | otherwise = kmeans' points moe cluster means'
    where converged = has_converged moe means' $ mean:means
          means'    = [ let s = [ points !! idx | idx <- indices c ] in mean' s | c <- nub clusters ]
          indices c = elemIndices c clusters
          clusters  = map (\p -> nearest p mean means) points
          mean' set = map ((1/size) *) $ map sum $ transpose set
            where size = fromIntegral $ length set
