module Cluster.Utils
( closest_pair
, merge
, total_cost
, cost
, has_converged
, nearest
, euclidean
, sample
) where

import qualified Data.Random.Extras as Sample (sample)
import Data.Random
  ( MonadRandom
  , runRVar
  , StdRandom(..)
  )
import Data.List ((\\))

closest_pair
  :: (Ord a, Floating a)
  => [[a]]
  -> ([a], [a])
closest_pair points@(p:ps)
  | length points /= 1 = closest
  | otherwise          = (p, p)
  where (_, closest) = foldl (\(m, p) (m', p') -> min' m m' p p' ) (inf, nil) products
        inf       = 10^17
        nil       = ([], [])
        products  = [ (euclidean a b, (a, b)) | a <- points, b <- points \\ [a] ]
        min' m m' p p'
          | m' < m    = (m', p')
          | otherwise = (m, p)

merge
  :: (Floating a)
  => ([a], [a])
  -> [a]
merge (x, y) = map (\(x', y') -> (x' + y') / 2) $ zip x y

total_cost
  :: (Ord a, Floating a)
  => [[a]]
  -> [[a]]
  -> a
total_cost [] _ = 0
total_cost (p:ps) medoids = cost p medoids + total_cost ps medoids

cost
  :: (Ord a, Floating a)
  => [a]
  -> [[a]]
  -> a
cost p (m:ms) = euclidean p $ nearest p m ms

has_converged
  :: (Ord a, Floating a)
  => a
  -> [[a]]
  -> [[a]]
  -> Bool
has_converged _ [] _ = True
has_converged _ _ [] = True
has_converged moe (x:xs) (y:ys)
  | (d <= moe && d >= -moe) = has_converged moe xs ys
  | otherwise               = False
    where d = euclidean x y

nearest
  :: (Ord a, Floating a)
  => [a]
  -> [a]
  -> [[a]]
  -> [a]
nearest _ n [] = n
nearest p n (m:ms)
  | euclidean p m < euclidean p n = nearest p m ms
  | otherwise                     = nearest p n ms

euclidean
  :: (Floating a)
  => [a]
  -> [a]
  -> a
euclidean a b = sqrt $ foldl (\acc (a', b') -> (a' - b')^2 + acc) 0.0 $ zip a b

sample
  :: (MonadRandom m)
  => Int
  -> [t]
  -> m [t]
sample s xs = sampled
  where sampled = runRVar (Sample.sample s xs) StdRandom
