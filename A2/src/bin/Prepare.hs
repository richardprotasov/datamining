import System.Environment (getArgs)

import Cluster.Preprocess
import Cluster.Utils (sample)
import System.IO
import Data.List
  ( sortBy
  , sort
  , group
  , (\\)
  )
import Data.Ord
import Data.Maybe

computeSizes :: (String, String) -> (Int, Int)
computeSizes (total, training) = (convert total', convert training')
  where total'    = read total :: Double
        training' = total' * (read training :: Double)
        convert v = fromInteger $ round v

popular
  :: Int
  -> [Pokemon]
  -> [Pokemon]
popular count pokemon = (concat . take count . order) pokemon
  where order = sortBy (flip $ comparing length) . group . sort

superRare
  :: [Pokemon]
  -> [Pokemon]
superRare pokemon = filter (flip elem ids . fromJust . getNScale . pokemon_id) pokemon
  where ids =
          [ 132.0
          , 150.0
          , 122.0
          , 131.0 ]

rare
  :: [Pokemon]
  -> [Pokemon]
rare pokemon = filter (flip elem ids . fromJust . getNScale . pokemon_id) pokemon
  where ids =
          [ 108
          , 89
          , 130
          , 137 ]

instance Eq Pokemon where
  (==) this that = fromJust $ (==) <$> val this <*> val that
    where val = getNScale . pokemon_id

instance Ord Pokemon where
  compare this that = fromJust $ compare <$> val this <*> val that
    where val = getNScale . pokemon_id

main = do
  pokemon <- fmap (rare . clean . toList) <$> open "data/pokemon.csv"

  case pokemon of
    Left reason -> do
      putStrLn "Error preparing datasets"
      print reason
    Right pokemon' -> do

      -- Gather selected sizes
      [total, training] <- getArgs

      let (total', training') = computeSizes (total, training)

      -- Seperate out new datasets
      cleanAll <- sample total' pokemon'
      cleanTraining <- sample training' cleanAll
      let cleanTesting = cleanAll \\ cleanTraining

      let fTraining = "data/pokemon.training.txt"
      let fTesting  = "data/pokemon.testing.txt"
      let fAll      = "data/pokemon.clean.txt"

      -- Store new datasets
      writeFile fTraining $ show cleanTraining
      writeFile fTesting  $ show cleanTesting
      writeFile fAll      $ show cleanAll
