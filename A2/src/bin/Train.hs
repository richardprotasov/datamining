import Cluster.Preprocess
  ( Pokemon
  , toList
  , open
  , flattenAll
  , allFeatures
  , aFewFeatures
  , aFewFeaturesButId
  )
import Cluster.Algorithms
import System.IO
import Data.Maybe

openPokemon
  :: FilePath
  -> IO [Pokemon]
openPokemon file =
  openFile file ReadMode >>= fmap read . hGetContents

main = do
  -- Gather together processed dataset for training
  pokemon <- openPokemon "data/pokemon.training.txt"

  -- Convert data types in accordance to algorithms
  let dataset = fromJust <$> flattenAll aFewFeaturesButId pokemon

  -- Create trained models
  kmmodel  <- kmeans dataset 0.0001 5
  pammodel <- pam dataset 0.0001 5
  -- let hbumodel = hbu dataset

  -- and save
  writeFile "data/pokemon.km.txt" (show kmmodel)
  writeFile "data/pokemon.pam.txt" (show pammodel)
  -- writeFile "data/pokemon.hbu.txt" (show hbumodel)
