import Cluster.Preprocess
  ( Pokemon
  , toList
  , open
  , flattenAll
  , allFeatures
  , aFewFeatures
  , aFewFeaturesButId
  )
import Cluster.Algorithms (classify)
import System.IO
import Control.Applicative
import Data.List
  ( concat
  , intersperse
  , elemIndex )
import Data.Maybe

openModel
  :: (Read a, Ord a, Floating a)
  => FilePath
  -> IO [[a]]
openModel file =
  openFile file ReadMode >>= fmap read . hGetContents

openPokemon
  :: FilePath
  -> IO [Pokemon]
openPokemon file =
  openFile file ReadMode >>= fmap read . hGetContents

header
  :: String
header = (concat . intersperse ",") fields
  where fields =
          [ "algorithm"
          , "cluster"
          , "pokemon"
          , "gym_distance"
          , "population"
          , "temperature"
          , "hour"
          , "terrain" ]

format
  :: (Show a, Ord a, Floating a)
  => String
  -> [[a]]
  -> [a]
  -> [a]
  -> String
format alg model example predicted = (concat . intersperse ",") formatted
  where formatted =
          [ alg
          , show (cluster + 1) ] ++ example'
        (Just cluster) = elemIndex predicted model
        example' = map show example

main = do
  kmmodel  <- openModel "data/pokemon.km.txt"
  pammodel <- openModel "data/pokemon.pam.txt"
  -- hbumodel <- openModel "data/pokemon.hbu.txt"
  pokemon  <- map fromJust . flattenAll aFewFeatures <$> openPokemon "data/pokemon.testing.txt"

  let predictionsKm  = classify kmmodel  . tail <$> pokemon
      predictionsPam = classify pammodel . tail <$> pokemon
      -- predictionsHbu = classify hbumodel <$> pokemon
      pokemon'       = ZipList pokemon
      csvKm          = getZipList $ format "km"  kmmodel  <$> pokemon' <*> ZipList predictionsKm
      csvPam         = getZipList $ format "pam" pammodel <$> pokemon' <*> ZipList predictionsPam
      -- csvHbu         = getZipList $ format "hbu" hbumodel <$> pokemon' <*> ZipList predictionsHbu

  writeFile "data/compare.csv" $ unlines [header, unlines $ csvPam ++ csvKm]
